﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.2.23.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 09/07/2021 8:17:47
-- Server version: 5.5.5-10.4.10-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS productos;

CREATE DATABASE IF NOT EXISTS productos
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci;

--
-- Set default database
--
USE productos;

--
-- Create table `usuarios`
--
CREATE TABLE IF NOT EXISTS usuarios (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  email varchar(50) NOT NULL,
  username varchar(55) NOT NULL,
  authKey varchar(255) DEFAULT NULL,
  activo int(1) NOT NULL DEFAULT 0,
  password varchar(255) DEFAULT NULL,
  accessToken varchar(255) DEFAULT NULL,
  role int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create index `UK_usuarios_email` on table `usuarios`
--
ALTER TABLE usuarios
ADD UNIQUE INDEX UK_usuarios_email (email);

--
-- Create index `UK_usuarios_username` on table `usuarios`
--
ALTER TABLE usuarios
ADD UNIQUE INDEX UK_usuarios_username (username);

--
-- Create table `productos`
--
CREATE TABLE IF NOT EXISTS productos (
  idproducto int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  precioBase float DEFAULT NULL,
  fechaPublicacion date DEFAULT NULL,
  foto varchar(100) DEFAULT NULL,
  vendido tinyint(1) DEFAULT NULL,
  fechaVenta date DEFAULT NULL,
  usuarioPublica int(11) DEFAULT NULL,
  PRIMARY KEY (idproducto)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create foreign key
--
ALTER TABLE productos
ADD CONSTRAINT fkproductosusuarios FOREIGN KEY (usuarioPublica)
REFERENCES usuarios (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `mensajes`
--
CREATE TABLE IF NOT EXISTS mensajes (
  idmensaje int(11) NOT NULL AUTO_INCREMENT,
  descripcion varchar(512) DEFAULT NULL,
  fechaPublicacion date DEFAULT NULL,
  hora time DEFAULT NULL,
  producto int(11) DEFAULT NULL,
  usuario int(11) DEFAULT NULL,
  PRIMARY KEY (idmensaje)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create foreign key
--
ALTER TABLE mensajes
ADD CONSTRAINT fkmensajesproductos FOREIGN KEY (producto)
REFERENCES productos (idproducto) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE mensajes
ADD CONSTRAINT fkmensajesusuarios FOREIGN KEY (usuario)
REFERENCES usuarios (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table usuarios
--
INSERT INTO usuarios VALUES
(1, 'ramon', 'abramo', 'RAMONABRAMO@GMAIL.COM', 'ramonabramo', '', 1, '$2y$13$IV5m6RXdTjO8l0f30FtChe0PKG3yQWqTaT5kNIhibqlx7UJ/FE0j6', '', 1),
(3, 'ana', NULL, 'clasealpeformacion2021@gmail.com', 'ana', 'IutZo6GIIkTMJgymUnwZG2Bg6a6vZGyv', 1, '$2y$13$8ILS9FdQHB4yLt.dZ7aULeJGkn85fsJPnGQvhepMysrQUClmBnA/O', NULL, 1);

-- 
-- Dumping data for table productos
--
INSERT INTO productos VALUES
(1, 'pizarra', 600, '2020-07-09', '1pizarra.png', 0, NULL, 1),
(2, 'pizarra1', 1000, NULL, '2pizarra.jpg', NULL, NULL, 1);

-- 
-- Dumping data for table mensajes
--
INSERT INTO mensajes VALUES
(1, 'Estamos probando', '2021-07-09', '07:30:00', 2, 1),
(2, 'otro mensaje', '2021-07-09', '07:30:00', 2, 3);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;