<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $idproducto
 * @property string $nombre
 * @property float|null $precioBase
 * @property string|null $fechaPublicacion
 * @property string|null $foto
 * @property int|null $vendido
 * @property string|null $fechaVenta
 * @property int|null $usuarioPublica
 *
 * @property Mensajes[] $mensajes
 * @property Usuarios $usuarioPublica0
 */
class Productos extends \yii\db\ActiveRecord
{
     public $actualizarFoto;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['precioBase'], 'number'],
            [['fechaPublicacion', 'fechaVenta'], 'safe'],
            [['vendido', 'usuarioPublica'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['foto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],  
            [['usuarioPublica'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuarioPublica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproducto' => 'Idproducto',
            'nombre' => 'Nombre',
            'precioBase' => 'Precio Base',
            'fechaPublicacion' => 'Fecha Publicacion',
            'foto' => 'Foto',
            'vendido' => 'Vendido',
            'fechaVenta' => 'Fecha Venta',
            'usuarioPublica' => 'Usuario Publica',
        ];
    }

    /**
     * Gets query for [[Mensajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMensajes()
    {
        return $this->hasMany(Mensajes::className(), ['producto' => 'idproducto']);
    }

    /**
     * Gets query for [[UsuarioPublica0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioPublica0()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuarioPublica']);
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        // poder introducir la fecha en castellano
        if(!empty($this->fechaPublicacion)){
            $this->fechaPublicacion= \DateTime::createFromFormat("d/m/Y", $this->fechaPublicacion)->format("Y/m/d");
        }
        
        if(!empty($this->fechaVenta)){
            $this->fechaVenta= \DateTime::createFromFormat("d/m/Y", $this->fechaVenta)->format("Y/m/d");
        }
        
        
        
        /* tema subida archivos */
        $this->actualizarFoto=TRUE;
        /* si no ha subido ningun fichero recupero el valor anterior de los ficheros */
        if(!isset($this->foto)){
            $this->foto=$this->getOldAttribute("foto");
            $this->actualizarFoto=FALSE;
        }
        
        return true;
        
    }

    public function afterSave($insert, $changeAttributes) {

        /* comprobar si ha seleccionado algun archivo */
        /* ademas comprobamos que sea un fichero lo que me llegue*/
        /* esto es para que si estoy borrando no me de problemas */
        /* llegaria un string en ese caso */
        
        if($this->actualizarFoto && is_object($this->foto)){
            
            // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
            $this->foto->saveAs('imgs/id_' . $this->idproducto . '_' . iconv('UTF-8','ISO-8859-1',$this->foto->name), false);
            $this->foto = 'id_' . $this->idproducto . '_' . iconv('UTF-8','ISO-8859-1', $this->foto->name);
        }
        
        //almacenar los cambios
        $this->updateAttributes(["foto"]);
    }
    
    
     public function getFoto() {
       return Yii::$app->request->getBaseUrl() . '/imgs/' . $this->foto;
    }
    
    public function afterFind() {
        parent::afterFind();
        // los listados de fecha me salen en castellano
       
        if(!empty($this->fechaPublicacion)){
            $this->fechaPublicacion=Yii::$app->formatter->asDate($this->fechaPublicacion, 'php:d/m/Y');
        }
        
        if(!empty($this->fechaVenta)){
            $this->fechaVenta=Yii::$app->formatter->asDate($this->fechaVenta, 'php:d/m/Y');
        }
        
    }
    
    public function eliminarFoto(){
        unlink('imgs/' . $this->foto);
        $this->foto="";
        $this->updateAttributes(["foto"]);
        
    }


    
    
}
