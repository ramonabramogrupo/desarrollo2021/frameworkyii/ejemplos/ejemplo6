<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensajes".
 *
 * @property int $idmensaje
 * @property string|null $descripcion
 * @property string|null $fechaPublicacion
 * @property string|null $hora
 * @property int|null $producto
 * @property int|null $usuario
 *
 * @property Productos $producto0
 * @property Usuarios $usuario0
 */
class Mensajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mensajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaPublicacion', 'hora'], 'safe'],
            [['producto', 'usuario'], 'integer'],
            [['descripcion'], 'string', 'max' => 512],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto' => 'idproducto']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmensaje' => 'Idmensaje',
            'descripcion' => 'Descripcion',
            'fechaPublicacion' => 'Fecha Publicacion',
            'hora' => 'Hora',
            'producto' => 'Producto',
            'usuario' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Producto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Productos::className(), ['idproducto' => 'producto']);
    }

    /**
     * Gets query for [[Usuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario']);
    }
}
