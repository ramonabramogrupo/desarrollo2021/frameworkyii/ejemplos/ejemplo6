<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mensajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mensajes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaPublicacion')->textInput() ?>

    <?= $form->field($model, 'hora')->textInput() ?>

    
    <?= $form->field($model, 'producto')->dropDownList(
         yii\helpers\ArrayHelper::map(app\models\Productos::find()->all(),'idproducto','nombre'),
        ['prompt'=>'Selecciona un producto']
        );?>


    <?= $form->field($model, 'usuario')->dropDownList(
         yii\helpers\ArrayHelper::map(app\models\Usuarios::find()->all(),'id','nombre'),
        ['prompt'=>'Selecciona un usuario']
        );?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
