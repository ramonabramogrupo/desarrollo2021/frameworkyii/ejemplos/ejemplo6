<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mensajes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mensajes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mensajes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idmensaje',
            'descripcion',
            //'fechaPublicacion',
            [
                'attribute'=>'fechaPublicacion',
                'format' =>  ['date', 'php:d-m-Y'],
                'headerOptions' => ['width' => '200'],
            ],
            'hora',
            //'producto',
            //'producto0.nombre',
            [
                'label'=>'nombre del producto',
                'attribute'=>'producto',
                'format' => 'text',
                'content'=>function($data){
                    return $data->producto0->nombre;
                }
            ],
            [
                'label'=>'Imagen del producto',
                'attribute'=>'producto',
                'format'=>'raw',
                'contentOptions'=>['style'=>'text-align:center'],
                'content'=>function($d){
        
                    return Html::img("@web/imgs/" . $d->producto0->foto,['width'=>'100px']);
                }
            ],                    
            //'usuario',
            //'usuario0.nombre',
            [
                'attribute'=>'usuario',
                'label' => 'Nombre del usuario',
                'format' => 'text',
                'content'=>function($data){
                    return $data->usuario0->nombre;
                }
            ],
        
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
