<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;


/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precioBase')->textInput() ?>

    <?php    
    echo '<label class="control-label">Fecha Publicacion</label>';
    echo DatePicker::widget([
        'model' => $model, 
        'attribute' => 'fechaPublicacion',
        'language' => 'es',
        'options' => ['placeholder' => 'Introduce fecha'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd/mm/yyyy',
            'autoclose' => true,
        ]
    ]);
    ?>


    <?= $form->field($model, 'foto')->fileInput() ?>
    
    <?php
    if (!empty($model->foto)) {
        echo Alert::widget([
            'body' => Html::img($model->getFoto(), [
                'class' => 'img-responsive img-thumbnail',
            ]),
            'closeButton' => [
                'tag' => 'a',
                'href' => \yii\helpers\Url::to(['productos/eliminar', 'id' => $model->idproducto]),
            ],
        ]);
    }
    ?>


    <?= $form->field($model, 'vendido')->textInput() ?>

        
    <?php    
    echo '<label class="control-label">Fecha Venta</label>';
    echo DatePicker::widget([
        'model' => $model, 
        'attribute' => 'fechaVenta',
        'language' => 'es',
        'options' => ['placeholder' => 'Introduce fecha'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd/mm/yyyy',
            'autoclose' => true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'usuarioPublica')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
